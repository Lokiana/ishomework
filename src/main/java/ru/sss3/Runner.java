package ru.sss3;

import ru.sss3.crawler.Crawler;
import ru.sss3.pr.PageRankCalculator;
import ru.sss3.pr.PageRankData;

import java.util.Collection;
import java.util.stream.IntStream;

/**
 * основной класс для запуска проекта
 */
public class Runner {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Not found start site");
        }

        final String url = args[0];

        if (args.length > 1) {
            IntStream.range(1, args.length)
                    .mapToObj(i -> args[i])
                    .map(command -> command.split("="))
                    .forEach(command -> {
                        switch (command[0].toLowerCase()) {
                            case Keys.CRAWLER_SPARSE :
                                Keys.put(Keys.CRAWLER_SPARSE, Boolean.TRUE);
                                break;
                            case Keys.CRAWLER_MAX_SIZE :
                                Keys.put(Keys.CRAWLER_SPARSE, command[1]);
                                break;
                            case Keys.CRAWLER_THREADS :
                                Keys.put(Keys.CRAWLER_THREADS, command[1]);
                                break;
                            case Keys.PRC_THREADS :
                                Keys.put(Keys.PRC_THREADS, command[1]);
                                break;
                            default :
                                throw new IllegalArgumentException("Not found argument " + command[0]);
                        }
                    });
        }

        final long start = System.currentTimeMillis();
        final Collection<PageRankData> result = PageRankCalculator.newInstance().calculate(Crawler.newInstance().work(url));
        final long end = System.currentTimeMillis();
        result.forEach(r -> System.out.println(r.page() + " : " + r.rank()));
        System.out.println("\n time of calculate " + (end - start) + "ms");
    }

}
