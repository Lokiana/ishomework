package ru.sss3.crawler.result;

import org.apache.commons.lang3.StringUtils;
import ru.sss3.Keys;

import java.util.Collection;

/**
 * интерфейс описывающий поведение стрыктуры данных
 */
public interface CrawlerResult {

    int DEFAULT_SIZE = 100;

    /**
     * Add link to store
     * @param from link
     * @param to link
     * @return false - if store is filled, otherwise - true
     */
    boolean addLink(String from, String to);

    /**
     * @return all links in store
     */
    Collection<String> allLinks();

    /**
     * @param page link
     * @return all links having a link to this page
     */
    Collection<String> in(String page);

    /**
     * @param page link
     * @return all links on this page
     */
    Collection<String> out(String page);

    static CrawlerResult newInstance() {
        final String maxSize = Keys.get(Keys.CRAWLER_MAX_SIZE);
        boolean useSparse = !StringUtils.isEmpty(Keys.get(Keys.CRAWLER_SPARSE));
        final int size = StringUtils.isNumeric(maxSize) ? Integer.valueOf(maxSize) : DEFAULT_SIZE;
        return useSparse ? new SparseMatrixCrawlerResult(size) : new ArrayBasedCrawlerResult(size);
    }

}
